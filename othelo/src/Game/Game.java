package Game;


public class Game {
	public final static int BLACK = 1; // Declare state of each square
	public final static int WHITE = -1;
	public final static int EMPTY = 0;
	public final static int OFFBOARD = -2;

	public final static int WIDTH = 10;
	public final static int HEIGHT = 10;

	public final static int END = -1;
	public final int board[][] = new int[WIDTH][HEIGHT];
	public Game() {
		initGame();
	}
	public void initGame() {
		for (int i = 0; i < WIDTH; i++) {
			board[i][0] = OFFBOARD;
			board[i][WIDTH - 1] = OFFBOARD;
			board[0][i] = OFFBOARD;
			board[HEIGHT - 1][i] = OFFBOARD;
		}

		for (int i = 1; i < HEIGHT - 1; i++)
			for (int j = 1; j < WIDTH - 1; j++)
				board[i][j] = EMPTY;

		board[HEIGHT / 2 - 1][WIDTH / 2 - 1] = WHITE;
		board[HEIGHT / 2][WIDTH / 2 - 1] = BLACK;
		board[HEIGHT / 2 - 1][WIDTH / 2] = BLACK;
		board[HEIGHT / 2][WIDTH / 2] = WHITE;
	}

	public Game(Game another) {
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				this.board[i][j] = another.board[i][j];
			}
		}
	}
	public boolean legalMove(int r, int c, int color, boolean flip) {
		boolean legal = false;
		if (board[r][c] == EMPTY) {
			int posX;
			int posY;
			boolean found;
			int current;
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					posX = c + x;
					posY = r + y;
					found = false;
					current = board[posY][posX];
					if (current == OFFBOARD || current == EMPTY || current == color) {
						continue;
					}

					while (!found) {
						posX += x;
						posY += y;
						current = board[posY][posX];
						if (current == color) {
							found = true;
							legal = true;
							if (flip) {
								posX -= x;
								posY -= y;
								current = board[posY][posX];

								while (current != EMPTY) {
									board[posY][posX] = color;
									posX -= x;
									posY -= y;
									current = board[posY][posX];
								}
							}
						}
						else if (current == OFFBOARD || current == EMPTY) {
							found = true;
						}
					}
				}
			}
		}

		return legal;
	}

	public Move pointMove(int r, int c, int color, boolean flip, int[][] point) {
		Move newMove = new Move();

		if (board[r][c] == EMPTY) {
			int posX;
			int posY;
			boolean found;
			int current;
			int sum;

			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					posX = c + x;
					posY = r + y;
					found = false;
					current = board[posY][posX];
					sum = 0;
					if (current == OFFBOARD || current == EMPTY || current == color) {
						continue;
					} else {
						sum += point[posY][posX];
					}

					while (!found) {
						posX += x;
						posY += y;
						current = board[posY][posX];

						if (current == color) {
							found = true;
							newMove.legal = true;
							newMove.x = c;
							newMove.y = r;
							newMove.points += point[c][r];

							if (flip) {
								posX -= x;
								posY -= y;
								current = board[posY][posX];

								while (current != EMPTY) {
									board[posY][posX] = color;
									posX -= x;
									posY -= y;
									current = board[posY][posX];
								}
							}
						} else if (current == OFFBOARD || current == EMPTY) {
							sum = 0;
							found = true;
						} else {
							sum += point[posY][posX];
						}
					}
					newMove.points += sum;
				}
			}
		}
		return newMove;
	}
	public void printBoard() {
		for (int i = 1; i <= HEIGHT - 2; i++) {
			for (int j = 1; j <= WIDTH - 2; j++) {
				System.out.print("[" + board[i][j] + "]");
			}
			System.out.println();
		}
	}
}
