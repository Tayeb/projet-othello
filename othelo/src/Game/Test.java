package Game;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Test {
	  private static int delay=0;
	  static int niv;
	  private static JComboBox petList;
	  private static Othello content;
	  public static JFrame window1=new JFrame("IA vs IA");
	  public static JFrame window2=new JFrame("Joueur vs IA");
	  public static JPanel p1;
	  public static JPanel p2;
	  public static JPanel p;
	  public static JComboBox petList1 ;
	  public static JComboBox petList2 ;
	  private static JToggleButton niv1;
	  private static JToggleButton niv2;
	  private static JToggleButton niv3;

	  
      public static void main(String [] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
    	UIManager.setLookAndFeel(
                "com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
        JFrame fenetre = new JFrame("Othello");
		JPanel panel = new JPanel();
		
		fenetre.setTitle("Othello"); 
		fenetre.setSize(500,557);
		fenetre.setLocation(100,100);            
		fenetre.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	 
	    JPanel pan = new JPanel();
	    pan.setBackground(new Color(19, 137, 199));        
	    JLabel label = new JLabel("Bienvenue au jeu othello");
	    label.setFont(new java.awt.Font("Bugfast", 0, 30));
	    label.setBounds(20, 20, 460, 60);

	    pan.add(label);
	    
	    JButton iavsia = new JButton("star");
	    
	    pan.setLayout(null);
	    int style = Font.BOLD | Font.ITALIC;

	    iavsia.setFont(new java.awt.Font("Bugfast", 0, 34)); // NOI18N
	    iavsia.setForeground(new java.awt.Color(0, 51, 255));
	    iavsia.setText("IA VS IA");
	    iavsia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNiveau1ActionPerformed(evt);
            }
        });
	    iavsia.setBounds(120, 150, 260, 60);
	    pan.add(iavsia);
	    JButton iavsjoueur = new JButton();
	    iavsjoueur.setFont(new java.awt.Font("Bugfast", 0, 34)); // NOI18N
	    iavsjoueur.setForeground(new java.awt.Color(0, 51, 255));
	    iavsjoueur.setText("Joueur VS IA");
	    iavsjoueur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonIavsjoueurActionPerformed(evt);
            }
        });
	    iavsjoueur.setBounds(120, 240, 260, 60);
	    pan.add(iavsjoueur);
        
	    fenetre.setContentPane(pan);               
	    fenetre.setVisible(true);
        
      
    }
    private static void buttonStartJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonStartActionPerformed
        // TODO add your handling code here:
    	content = new Othello(0,niv,petList.getSelectedIndex());
    	content.setBounds(0, 33, 500, 500);
    	window2.getContentPane().add(content);
    	window2.repaint();

    	System.out.println("testt");
    }//GEN-LAST:event_buttonStartActionPerformed
    
    private static void buttonStartIAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonStartActionPerformed
        // TODO add your handling code here:
    	content = new Othello(1000,petList1.getSelectedIndex(),petList2.getSelectedIndex());   	
    	content.setBounds(0, 33, 600, 500);
    	window1.getContentPane().add(content, BorderLayout.CENTER);
    	window1.repaint();

    	System.out.println("testt");
    }//GEN-LAST:event_buttonStartActionPerformed
    
    private static void buttonNiveau1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNiveau1ActionPerformed
        // TODO add your handling code here:
    	
    	window1=new JFrame("Joeur vs IA");
    	String[] petStrings = { "MinMax","NegaMax", "AlphaBéta", "NAlphaBéta", "SSS*", "Random", "Point"};
    	petList1 = new JComboBox(petStrings);
   		petList2 = new JComboBox(petStrings);
   		
   	 JButton buttonStartIA = new JButton("Start");
     buttonStartIA.setBackground(new java.awt.Color(204, 0, 51));
     buttonStartIA.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
             buttonStartIAActionPerformed(evt);
         }
     });
     buttonStartIA.setBounds(0, 50, 80, 25);

       //content = new Othello(2000);
       
       JPanel p1 = new JPanel();
       p1.add(buttonStartIA);
       p1.add(new JLabel("Joeur 1 : "));
       p1.add(petList1);
       p1.add(new JLabel("Joeur 2 : "));
       p1.add(petList2);
       window1.add(p1);
       window1.getContentPane().add(p1, BorderLayout.NORTH);
       
      JPanel p2 = new JPanel();
       p2.add(new javax.swing.JButton("test2"));
       
       
      
    
       window1.setSize(500,557);
       window1.setLocation(100,100);            
       //window1.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
       window1.setVisible(true);
    }//GEN-LAST:event_buttonNiveau1ActionPerformed
    
    
    private static void buttonIavsjoueurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNiveau3ActionPerformed
        // TODO add your handling code here:
     window2 = new JFrame("Othello Game"); 
     int niv = 1; 
	 JButton buttonStartJ = new JButton("Start");
     buttonStartJ.setBackground(new java.awt.Color(204, 0, 51));
     buttonStartJ.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
             buttonStartJActionPerformed(evt);
         }
     });
     buttonStartJ.setBounds(0, 50, 80, 25);
     

     
     niv1 = new JToggleButton("Level1");
     niv1.setForeground(Color.white);
	 niv1.setBackground(new java.awt.Color(204, 0, 51));
	 niv1.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
        	 buttonNiveau11ActionPerformed(evt);
         }
     });
	 niv1.setBounds(0, 80, 10, 10);
     
	 niv2 = new JToggleButton("Level2");
	 niv2.setBackground(new java.awt.Color(204, 0, 51));
	 niv2.setForeground(Color.white);
	 niv2.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
        	 buttonNiveau2ActionPerformed(evt);
         }
     });
	 niv2.setBounds(0, 50, 80, 25);
     
	 niv3 = new JToggleButton("Level3");
	 niv3.setBackground(new java.awt.Color(204, 0, 51));
	 niv3.setForeground(Color.white);
	 niv3.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
        	 buttonNiveau3ActionPerformed(evt);
         }
     });
	 niv3.setBounds(0, 50, 80, 25);
	 String[] petStrings = { "MinMax","NegaMax", "AlphaBéta", "NAlphaBéta", "SSS*", "Random", "Point"};
	 petList = new JComboBox(petStrings);
	 petList.setBackground(new java.awt.Color(204, 0, 51));
	 petList.setForeground(Color.white);;

    
    window2 = new JFrame("Othello Game");
    p = new JPanel();
    p1 = new JPanel();
    buttonStartJ.setForeground(Color.white);
    p1.add(buttonStartJ);
    p1.add(niv1);
    p1.add(niv2);
    p1.add(niv3);
    p1.add(petList);
    p1.setBounds(0, 0, 500, 33);
    
    //window2.add(p1);
    window2.setLayout(null);
    window2.getContentPane().add(p1);
    
 
    window2.setSize(500,560);
    window2.setLocation(100,100);            
    //window2.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    window2.setVisible(true);
    }//GEN-LAST:event_buttonNiveau3ActionPerformed
    
 private static  void buttonNiveau11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNiveau1ActionPerformed
     // TODO add your handling code here:
     niv=1;
     niv2.setSelected(false);
     niv3.setSelected(false);
     System.out.println("niv : " +niv);
 }//GEN-LAST:event_buttonNiveau1ActionPerformed

 private static void buttonNiveau2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNiveau2ActionPerformed
     // TODO add your handling code here:
     niv=2;
     niv1.setSelected(false);
     niv3.setSelected(false);
     System.out.println("niv : " +niv);
 }//GEN-LAST:event_buttonNiveau2ActionPerformed

 private static void buttonNiveau3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNiveau3ActionPerformed
     // TODO add your handling code here:
     niv=3;
     niv1.setSelected(false);
     niv2.setSelected(false);
	 System.out.println("niv : " +niv);
 }//GEN-LAST:event_buttonNiveau3ActionPerformed
}