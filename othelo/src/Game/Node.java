package Game;

import java.util.ArrayList;

 

public class Node
{
	public Move last;
	public Game state;
	public int turn;
	
	public  int position = 0;
	public  int mobility = 0;
	public int end = -1;
	
	public Node parent;
	public ArrayList<Node> children = new ArrayList<Node>();
	
	static public enum Etat {
		Vivant,
		Resolu
	}
	
	private double sssValue;
	private Etat etat;
	public int name;
	public Node(Move last, Game state, int turn)
	{
		this.last = last;
		this.state = state;
		this.turn = turn;
	}
	
	public void addChild(Node newChild)
	{
		if (children == null)
		{
			System.out.println("Yes");
		}
		else
		{
			children.add(newChild);
		}
	}

	public double getSssValue() {
		return sssValue;
	}

	public void setSssValue(double sssValue) {
		this.sssValue = sssValue;
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}
	
	public static boolean isAnscentre(Node anc, Node desc) {
		if(desc.parent == null)
			return false;
		if(anc.name == desc.parent.name) {
			return true;
		}else {
			return isAnscentre(anc, desc.parent);
		}
	}
	
	public String toTuple() {
		return "<("+this.name+", "+this.etat+", "+this.getSssValue()+")>";
	}
}  