package Game;
import java.util.ArrayList;

import Strategy.Strategy;
import algorithme.Algorithme;

public class Player {
	
	
	FillPoints fillpoints;
	final double POSITIONWEIGHT = 5;
	final double MOBILITYWEIGHT = 15;
	final double ENDWEIGHT = 300;
	final double INFINITE = 100000000;
	int MAXDEPTH = 5;
	int player;
	Strategy strategy;
	
	public Player(int player, Strategy strategy)
	{
		this.player = player;
		this.strategy = strategy; 
	}
	
	
	public Game strategy(Game game, boolean done, int color) {
		return strategy.strategy(game, done, color);
	}
	public void printTree(Node parent)
	{
		ArrayList<Node> q = new ArrayList<Node>();
		Node temp = parent;
		for (Node n : temp.children)
		{
			q.add(n);
		}
		
		System.out.println("Node: " + temp.last.x + " " + temp.last.y);
		
		while (!q.isEmpty())
		{
			temp = (Node)q.remove(0);
			System.out.println("Node: " + temp.last.x + " " + temp.last.y +
								" (Parent: " + temp.parent.last.x + " " + temp.parent.last.y + ")");
			System.out.println("Position: " + temp.position);
			System.out.println("Mobility: " + temp.mobility);
			System.out.println("End: " + temp.end);
			
			for (Node n : temp.children)
			{
				q.add(n);
			}
		}
	}
	public void printScores()
	{
		for (int i = 0; i <= 9; i++)
		{
			for (int j = 0; j <= 9; j++)
			{
				System.out.print("[" + fillpoints.pointTable[i][j] + "]");
			}
			System.out.println();
		}
	}

}
