package Game;

import java.util.ArrayList;

import algorithme.Algorithme;
  

public class White 
{
	final static int pointTable[][] = new int[Game.WIDTH][Game.HEIGHT];
	
	final int CORNER = 50;
	final int DIAGONAL = -10;
	final int SECOND = -1;
	final int THIRD = 5;
	final int FOURTH = 2;
	final int COMMON = 1;
	final int STARTER = 0;
	final double POSITIONWEIGHT = 5;
	final double MOBILITYWEIGHT = 15;
	final double ENDWEIGHT = 300;
	final double INFINITE = 100000000;
	int MAXDEPTH = 5;
	
	
	private Algorithme algorithme;
	public White(Algorithme algorithme, int depth)
	{
		this.algorithme = algorithme;
		this.MAXDEPTH = depth;
		fillPoints();
	}
	public void fillPoints()
	{
		pointTable[1][1] = CORNER;
		pointTable[2][2] = DIAGONAL;
		pointTable[1][2] = pointTable[2][1] = SECOND;
		pointTable[1][3] = pointTable[3][1] = THIRD;
		pointTable[1][4] = pointTable[4][1] = FOURTH;
		pointTable[2][3] = pointTable[2][4] = pointTable[3][2] = pointTable[3][3] = 
		pointTable[3][4] = pointTable[4][2] = pointTable[4][3] = COMMON;
		pointTable[4][4] = STARTER;
		
		for (int i = Game.HEIGHT/2; i <= Game.WIDTH-2; i++)
		{
			for (int j = 1; j <= (Game.HEIGHT-2)/2; j++)
			{
				pointTable[j][i] = pointTable[(j)][Game.HEIGHT-1-i];
			}
		}	

		for (int i = 1; i <= Game.WIDTH-2; i++)
		{
			for (int j = Game.HEIGHT/2; j <= Game.HEIGHT-2; j++)
			{
				pointTable[j][i] = pointTable[Game.HEIGHT-1-j][(i)];
			}
		}		

		for (int i = 0; i <= Game.HEIGHT-1; i++)
		{
			pointTable[i][0] = pointTable[i][Game.HEIGHT-1] = pointTable[0][i] = pointTable[Game.HEIGHT-1][i] = STARTER;     
		}
	}
	
    public Game strategy(Game game, boolean done, int color) {
        
        return searchStrategy(game,done,color);
    }

    public Game randStrategy(Game game, boolean done, int color) {

        int row = (int)(Math.random()*(game.HEIGHT-2)) + 1;
        int column = (int)(Math.random()*(game.WIDTH-2)) + 1;
        
        while (!done && !game.legalMove(row,column,color,true)) {
            row = (int)(Math.random()*(game.HEIGHT-2)) + 1;
            column = (int)(Math.random()*(game.WIDTH-2)) + 1;
        }
        
        if (!done) 
            game.board[row][column] = color;

        return game;
    }
    public Game pointStrategy(Game game, boolean done, int color) 
	{
		if (!done)
		{
			Move bestMove = new Move();
			Move currentMove = new Move();
			
			for (int i = 1; i <= Game.HEIGHT-2; i++)
			{
				for (int j = 1; j <= game.WIDTH-2; j++)
				{
					currentMove = game.pointMove(j, i, color, false, pointTable);
					
					if (currentMove.legal)
					{
						if (currentMove.points > bestMove.points || !bestMove.legal)
						{
							bestMove = currentMove;
						}
					}
				}
			}
			
			if (bestMove.legal)
			{
				game.pointMove(bestMove.y, bestMove.x, color, true, pointTable);
				game.board[bestMove.y][bestMove.x] = color;
			}
		}

        return game;
    }
	
	public Game searchStrategy(Game game, boolean done, int color) 
	{	
		if (!done)
		{
			Node currentState = new Node(new Move(), game, color);
			currentState = buildTree(currentState, MAXDEPTH);
			
			Move bestMove = algorithme.execute(currentState, color);
			
			if (bestMove.legal)
			{
				game.pointMove(bestMove.y, bestMove.x, color, true, pointTable);
				game.board[bestMove.y][bestMove.x] = color;
			}
		}
		
		return game;
	}
	public Node buildTree(Node parent, int depth)
	{
		if (depth > 0 && parent.end == Game.END)
		{
			depth--;
			
			int nextTurn;
			if (parent.turn == Game.BLACK) 
				nextTurn = Game.WHITE;
			else 
				nextTurn = Game.BLACK;
			for (int i = 1; i <= Game.HEIGHT-2; i++)
			{
				for (int j = 1; j <= Game.WIDTH-2; j++)
				{
					Move currentMove = parent.state.pointMove(i, j, parent.turn, false, pointTable);
					
					if (currentMove.legal)
					{
						Game futureGame = new Game(parent.state);
						futureGame = makeMove(futureGame, false, parent.turn, currentMove);
						Node newNode = new Node(currentMove, futureGame, nextTurn);
						newNode.position = currentMove.points;
						newNode.mobility = mobilityCheck(futureGame, nextTurn);
						newNode.end =  endCheck(futureGame);
						parent.addChild(newNode);
						newNode.parent = parent;
					}
				}
			}
			if (depth > 0)
			{
				for (Node n : parent.children)
				{
					n = buildTree(n, depth);
				}
			}
			parent.mobility = parent.children.size();
		}
		return parent;
	}
	 
	public Game makeMove(Game game, boolean done, int color, Move move) 
	{
		if (!done)
		{
			if (move.legal)
			{
				game.pointMove(move.y, move.x, color, true, pointTable);
				game.board[move.y][move.x] = color;
			}
		}

        return game;
    }
	
	
    public int mobilityCheck(Game game, int color) 
	{
		int result = 0;
		
		for (int i=1; i<game.HEIGHT-1; i++)
		{
            for (int j=1; j<game.WIDTH-1; j++)
			{
				if ((game.legalMove(i, j, color, false)))
				{
                    result++;
				}
			}
		}

        return result;
    }
	
	
    public int endCheck(Game game) 
	{
		int result = Game.END;
		int whiteSum = 0;
		int blackSum = 0;
		
		for (int i=1; i<game.HEIGHT-1; i++)
		{
            for (int j=1; j<game.WIDTH-1; j++)
			{
				if ((game.legalMove(i, j, Game.BLACK, false)) ||
                   (game.legalMove(i, j, Game.WHITE, false)))
				{
                    result = Game.END;
					return result;
				}
				
				if (game.board[i][j] == Game.BLACK)
				{
					blackSum++;
				}
				else if (game.board[i][j] == Game.WHITE)
				{
					whiteSum++;
				}
			}
		}

		if (blackSum > whiteSum)
		{
			result = Game.BLACK;
		}
		else if (whiteSum > blackSum)
		{
			result = Game.WHITE;
		}
		else
		{
			result = 0;
		}
		
        return result;
    }
	
	
	public void printTree(Node parent)
	{
		ArrayList<Node> q = new ArrayList<Node>();
		Node temp = parent;
		for (Node n : temp.children)
		{
			q.add(n);
		}
		
		System.out.println("Node: " + temp.last.x + " " + temp.last.y);
		
		while (!q.isEmpty())
		{
			temp = (Node)q.remove(0);
			System.out.println("Node: " + temp.last.x + " " + temp.last.y +
								" (Parent: " + temp.parent.last.x + " " + temp.parent.last.y + ")");
			System.out.println("Position: " + temp.position);
			System.out.println("Mobility: " + temp.mobility);
			System.out.println("End: " + temp.end);
			
			for (Node n : temp.children)
			{
				q.add(n);
			}
		}
	}
	
	public void printScores()
	{
		for (int i = 0; i <= 9; i++)
		{
			for (int j = 0; j <= 9; j++)
			{
				System.out.print("[" + pointTable[i][j] + "]");
			}
			System.out.println();
		}
	}
}
