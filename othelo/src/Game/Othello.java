package Game;

import java.util.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import Strategy.PointStrategy;
import Strategy.RandStrategy;
import Strategy.Strategy;
import Strategy.WinnerStrategy;
import algorithme.AlphaBeta;
import algorithme.MinMax;
import algorithme.NegaAlphaBeta;
import algorithme.NegaMax;
import algorithme.SSSStar;

public class Othello extends JPanel {


	private static final long serialVersionUID = 1L;
	final static int BLACK = 1; // Declare state of each square
	final static int WHITE = -1;
	final static int EMPTY = 0;
	JButton j = new JButton("TESTTTT");
	// final static int OFFBOARD = -1;
	static int niv;
	private Thread t;
	Player black_;
	Player white_;
	private int tmpGlobal = 0;
	private int heures = 0;
	private int minutes = 0;
	private int seconde = 0;
	private String temps = "";
	private static Othello content;
	private static boolean start = false;
	private static JLabel timerLabel=new JLabel("test");

	private static JComboBox petList;

	private Game game = new Game(); // Game state
	private javax.swing.Timer timer;

	private static long startTime, stopTime, runTime = 0;
	private int turn = BLACK;
	private boolean black_done = false;
	private boolean white_done = false;


	public Othello(int delay, int niv, int alg) {
		t = new Thread() {
			public void run() {
				while(true) {
				tmpGlobal++;
				heures = tmpGlobal / 3600;
				minutes = (tmpGlobal % 3600) / 60;
				seconde = tmpGlobal % 60;
				if (heures < 10)
					temps = "0" + heures;
				else
					temps = "heures";
				if (minutes < 10)
					temps = temps + ":" + "0" + minutes;
				else
					temps = temps + ":" + minutes;
				if (seconde < 10)
					temps = temps + ":" + "0" + seconde;
				else
					temps = temps + ":" + seconde;
				timerLabel.repaint();

				try {
					t.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
			}
			
			};
			t.start();
			

		// Initialize the game state
		// initGame(game);
		turn = BLACK;

		// Run the game with GUI - computer vs. computer using a timer
		if (delay > 0) {
			// initIa(niv, alg);
			initPlayerIA(niv, alg);
			setBackground(new Color(19, 137, 199));
			timer = new javax.swing.Timer(delay, new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					playerMove();
					repaint();
				}
			});

			// Create the Start and Stop buttons
			JButton start = new JButton("play");
			start.setBounds(150, 30, 80, 25);
			add(start);
			start.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					timer.start();
				}
			});

			JButton stop = new JButton("Stop");
			stop.setBounds(260, 30, 80, 25);
			add(stop);
			stop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					timer.stop();
				}
			});
		}

		// Run the game with GUI - human vs. computer.
		// The human player always plays with the black discs.
		if (delay == 0) {
			// initPlayer(niv, alg);
			initIA(alg, niv);
			System.out.println("----------------player vs IA --------------");
			setBackground(new Color(19, 137, 199));
			addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent evt) {
					// Find out which square was clicked
					int x = evt.getX();
					int y = evt.getY();
					int screenWidth = getWidth();
					int screenHeight = getHeight();
					int column = (x * (game.WIDTH - 2)) / screenWidth + 1;
					int row = (y * (game.HEIGHT - 2)) / screenHeight + 1;

					if (!game.legalMove(row, column, BLACK, true))
						System.out.println("Not a legal move - try again!");
					else {
						game.board[row][column] = BLACK;
						repaint();
						black_done = true;
						for (int i = 1; i < game.HEIGHT - 1; i++)
							for (int j = 1; j < game.WIDTH - 1; j++)
								if (game.legalMove(i, j, BLACK, false))
									black_done = false;
						whiteMove();
					}
				}
			});
		}

		// Run the game without the GUI - as many times as specified in delay.
		if (delay < 0) {

			// Start timing how long it takes to play "delay" games
			startTime = new Date().getTime();

			// Keep track of how many wins each color has
			int white_won = 0;
			int black_won = 0;
			int ties = 0;

			// Play a bunch of games!
			for (int times = 0; times < -delay; times++) {
//				initGame(game);
				boolean done = false;
				white_done = false;
				black_done = false;

				while (!done) {
					playerMove();
					int bC = 0;
					int wC = 0;

					for (int i = 1; i < game.HEIGHT - 1; i++) {
						for (int j = 1; j < game.WIDTH - 1; j++) {
							if (game.board[i][j] == BLACK)
								bC++;
							else if (game.board[i][j] == WHITE)
								wC++;
						}
					}

					// Check if there are any more moves to make
					done = true;
					for (int i = 1; i < game.HEIGHT - 1; i++)
						for (int j = 1; j < game.WIDTH - 1; j++)
							if ((game.legalMove(i, j, BLACK, false)) || (game.legalMove(i, j, WHITE, false)))
								done = false;

					if (done)
						if (wC > bC) {
							white_won++;
						} else if (bC > wC) {
							black_won++;
						} else {
							ties++;
						}
				}
			}

			stopTime = new Date().getTime();
			runTime = (stopTime - startTime);


		}
	}

	public void playerMove() {

		if (turn == BLACK) {
			blackMove();
			turn = WHITE;
		} else {
			whiteMove();
			turn = BLACK;
		}
	}

    public JButton getJ() {
    	return j;
    }


	public void blackMove() {

		// Check if Black can move anywhere
		black_done = true;
		for (int i = 1; i < game.HEIGHT - 1; i++)
			for (int j = 1; j < game.WIDTH - 1; j++)
				if (game.legalMove(i, j, BLACK, false))
					black_done = false;

		// game = black.strategy(game, black_done, BLACK);
		game = black_.strategy(game, black_done, BLACK);
	}
	public void whiteMove() {

		// Check if White can move
		white_done = true;
		for (int i = 1; i < game.HEIGHT - 1; i++)
			for (int j = 1; j < game.WIDTH - 1; j++)
				if (game.legalMove(i, j, WHITE, false))
					white_done = false;

		game = white_.strategy(game, white_done, WHITE);
	}
	public void paintComponent(Graphics g) {

		super.paintComponent(g); // Fill panel with background color

		int width = getWidth();
		int height = getHeight();
		int xoff = width / (game.WIDTH - 2);
		int yoff = height / (game.HEIGHT - 2);

		int bCount = 0;
		int wCount = 0;
		g.setColor(Color.BLACK);
		g.setColor(new Color(214,211,206));
		g.fillRect(0, 0, 500, 60);
		g.setColor(Color.white);
		g.fillOval((8 * yoff) - yoff + 7, (1 * xoff) - xoff + 7, 30, 30);
		g.setColor(Color.black);
		g.fillOval((1 * yoff) - yoff + 7, (1 * xoff) - xoff + 7, 30, 30);
		for (int i = 2; i < game.HEIGHT - 1; i++) {
			for (int j = 1; j < game.WIDTH - 1; j++) {
				if (game.board[i][j] == BLACK) {
					g.setColor(Color.BLACK);
					g.fillOval((j * yoff) - yoff + 7, (i * xoff) - xoff + 7, 50, 50);

					bCount++;
				} else if (game.board[i][j] == WHITE) {
					g.setColor(Color.WHITE);
					g.fillOval((j * yoff) - yoff + 7, (i * xoff) - xoff + 7, 50, 50);
					wCount++;
				}
				// Show the legal moves for the current player
				if (turn == BLACK && game.legalMove(i, j, BLACK, false)) {
					g.setColor(Color.BLACK);
					g.fillOval((j * yoff + 29) - yoff, (i * xoff + 29) - xoff, 6, 6);
				}
				// If other player cannot move, current player cleans up
				if (turn == WHITE && game.legalMove(i, j, WHITE, false)) {
					g.setColor(Color.WHITE);
					g.fillOval((j * yoff + 29) - yoff, (i * xoff + 29) - xoff, 6, 6);
				}
			}
		}

		// Check if there are any more moves to make
		boolean done = true;
		for (int i = 1; i < game.HEIGHT - 1; i++)
			for (int j = 1; j < game.WIDTH - 1; j++)
				if ((game.legalMove(i, j, BLACK, false)) || (game.legalMove(i, j, WHITE, false)))
					done = false;

		timerLabel.setFont(new java.awt.Font("Bugfast", 0, 25));
	
		g.setFont(new java.awt.Font("Tempus Sans ITC", 0, 30));
		g.setColor(Color.black);
		g.drawString( bCount+"", 60, 33);
		g.drawString( wCount+"", 400, 33);
		
		timerLabel.setText(temps);
		int style = Font.BOLD | Font.ITALIC;

		timerLabel.setFont(new java.awt.Font("Bugfast", 0, 25));
		add(timerLabel);
		if (done) {
			g.setFont(new java.awt.Font("Tempus Sans ITC", 0, 30));
			if (wCount > bCount)
				g.drawString("VICTOIRE BLANC AVEC " + wCount, 70, 200);
			else if (bCount > wCount)
				g.drawString("VICTOIRE NOIR AVEC  " + bCount, 70, 200);
			else
				g.drawString("NULL", 10, 20);
		} 	
	}

	private static void buttonStartActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonStartActionPerformed
		// TODO add your handling code here:
		System.out.println(petList.getSelectedItem().toString());
	}// GEN-LAST:event_buttonStartActionPerformed

	private static void buttonNiveau1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonNiveau1ActionPerformed
		// TODO add your handling code here:

		String[] petStrings = { "MinMax", "NegaMax" };
		JComboBox petList1 = new JComboBox(petStrings);
		JComboBox petList2 = new JComboBox(petStrings);

		JFrame window = new JFrame("Othellonghjvb Game");
		JPanel p1 = new JPanel();
		p1.add(new JLabel("Joueur 1:"));
		p1.add(petList1);
		p1.add(new JLabel("Joueur 2:"));
		p1.add(petList2);
		window.add(p1);
		window.getContentPane().add(p1, BorderLayout.NORTH);

		JPanel p2 = new JPanel();
		p2.add(new javax.swing.JButton("test2"));
		window.getContentPane().add(content, BorderLayout.CENTER);


		window.setSize(500, 557);
		window.setLocation(100, 100);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}// GEN-LAST:event_buttonNiveau1ActionPerformed

	private static void buttonIavsjoueurActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonNiveau3ActionPerformed
		// TODO add your handling code here:
		int niv = 1;

		JButton niv1 = new JButton("Niveau 1");
		niv1.setBackground(new java.awt.Color(204, 0, 51));
		niv1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonNiveau11ActionPerformed(evt);
			}
		});
		niv1.setBounds(0, 50, 80, 25);

		JButton niv2 = new JButton("Niveau 2");
		niv2.setBackground(new java.awt.Color(204, 0, 51));
		niv2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonNiveau2ActionPerformed(evt);
			}
		});
		niv2.setBounds(0, 50, 80, 25);

		JButton niv3 = new JButton("Niveau 3");
		niv3.setBackground(new java.awt.Color(204, 0, 51));
		niv3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonNiveau3ActionPerformed(evt);
			}
		});
		niv3.setBounds(0, 50, 80, 25);
		String[] petStrings = { "MinMax", "NegaMax" };
		petList = new JComboBox(petStrings);

		JFrame window = new JFrame("Othello Game");
		JPanel p1 = new JPanel();
				p1.add(niv1);
		p1.add(niv2);
		p1.add(niv3);
		p1.add(petList);
		window.add(p1);
		window.getContentPane().add(p1, BorderLayout.NORTH);

		JPanel p2 = new JPanel();
		p2.add(new javax.swing.JButton("test2"));
		window.getContentPane().add(content, BorderLayout.CENTER);
		window.setSize(500, 557);
		window.setLocation(100, 100);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}

	private static void buttonNiveau11ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonNiveau1ActionPerformed
		// TODO add your handling code here:
		niv = 1;
		System.out.println("niv : " + niv);
	}// GEN-LAST:event_buttonNiveau1ActionPerformed

	private static void buttonNiveau2ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonNiveau2ActionPerformed
		// TODO add your handling code here:
		niv = 2;
		System.out.println("niv : " + niv);
	}// GEN-LAST:event_buttonNiveau2ActionPerformed

	private static void buttonNiveau3ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonNiveau3ActionPerformed
		// TODO add your handling code here:
		niv = 3;
		System.out.println("niv : " + niv);
	}// GEN-LAST:event_buttonNiveau3ActionPerformed

	/**************************************************/
	private static int getDepth(int niv) {
		return niv + 2;
	}

	private Strategy getStrategy(int algo, int depth) {
		switch (algo) {
		case 0:
			return new WinnerStrategy(new MinMax(), depth);
		case 1:
			return new WinnerStrategy(new NegaMax(), depth);
		case 2:
			return new WinnerStrategy(new AlphaBeta(), depth);
		case 3:
			return new WinnerStrategy(new NegaAlphaBeta(), depth);
		case 4:
			return new WinnerStrategy(new SSSStar(), depth);
		case 5:
			return new RandStrategy();
		case 6:
			return new PointStrategy();
		default:
			return new WinnerStrategy(new MinMax(), depth);
		}
	}

	private Strategy getStrategy(int algo) {
		return getStrategy(algo, 5);
	}

	private void initPlayerIA(int algo_1, int algo_2) {
		black_ = new Player(game.BLACK, getStrategy(algo_1));
		white_ = new Player(game.WHITE, getStrategy(algo_2));
	}

	private void initIA(int algo, int niv) {
		white_ = new Player(game.WHITE, getStrategy(algo, getDepth(niv)));
	}
	
}