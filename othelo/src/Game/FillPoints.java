package Game;

public class FillPoints {

	
	static private FillPoints instance;
	
	public final int pointTable[][] = new int[Game.WIDTH][Game.HEIGHT];

	private final  int CORNER = 50;
	private final  int DIAGONAL = -10;
	private final  int SECOND = -1;
	private final  int THIRD = 5;
	private final  int FOURTH = 2;
	private final  int COMMON = 1;
	private final  int STARTER = 0;

	private FillPoints() {
		fillPoints();
	}

	public static FillPoints getInstance() {
		if(instance == null)
			instance = new FillPoints();
		return instance;
	}
	private void fillPoints() {

		pointTable[1][1] = CORNER;
		pointTable[2][2] = DIAGONAL;
		pointTable[1][2] = pointTable[2][1] = SECOND;
		pointTable[1][3] = pointTable[3][1] = THIRD;
		pointTable[1][4] = pointTable[4][1] = FOURTH;
		pointTable[2][3] = pointTable[2][4] = pointTable[3][2] = pointTable[3][3] = pointTable[3][4] = pointTable[4][2] = pointTable[4][3] = COMMON;
		pointTable[4][4] = STARTER;
		for (int i = Game.HEIGHT / 2; i <= Game.WIDTH - 2; i++) {
			for (int j = 1; j <= (Game.HEIGHT - 2) / 2; j++) {
				pointTable[j][i] = pointTable[(j)][Game.HEIGHT - 1 - i];
			}
		}
		for (int i = 1; i <= Game.WIDTH - 2; i++) {
			for (int j = Game.HEIGHT / 2; j <= Game.HEIGHT - 2; j++) {
				pointTable[j][i] = pointTable[Game.HEIGHT - 1 - j][(i)];
			}
		}
		for (int i = 0; i <= Game.HEIGHT - 1; i++) {
			pointTable[i][0] = pointTable[i][Game.HEIGHT
					- 1] = pointTable[0][i] = pointTable[Game.HEIGHT - 1][i] = STARTER;
		}
	}
}
