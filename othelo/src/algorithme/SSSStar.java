package algorithme;

import java.util.ArrayList;

import Game.Move;
import Game.Node;
import Game.Node.Etat;

public class SSSStar extends Algorithme {

	/**
	 * Uses minimax algorithm to determine a move
	 *
	 * @param root
	 *            the root of the tree to be checked
	 * @param color
	 *            the player who is making the decision
	 *
	 * @return best the best course of action as indicated by minimax
	 */

	@Override
	public Move execute(Node root, int player) {
		FIFO file = new FIFO();
		Node bestNode = null;
		setEtatValue(root, Node.Etat.Vivant, INFINITE);
		// Check every node in children
		file.add(root);
		bestNode = root;
//		printTree(root, " ");
		while (!(file.getValueFirst() == root.name && file.getEtatFirst() == Node.Etat.Resolu)) {
			Node n = file.getFirst();
			double e = n.getSssValue();
			if (n.getEtat() == Node.Etat.Vivant) {
				if (n.children.size() == 0) { // feuille
					double value = checkValue(n, n.turn);
					if (value < e) {
						value = e;
						bestNode = n;
					}
					n.setSssValue(value);
					n.setEtat(Etat.Resolu);
					file.add(n);
				} else {
					if (player == n.turn) { // max
						for (Node fils : n.children) {
							setEtatValue(fils, Node.Etat.Vivant, e);
							file.add(fils);
						}
					} else { // min
						Node first = n.children.get(0);
						setEtatValue(first, Node.Etat.Vivant, e);
						file.add(first);
					}
				}
			} else { // {resolu}
				if (n.turn != player) { // min
					Node pere = setEtatValue(n.parent, Node.Etat.Resolu, e);
					file.add(pere);
					remove(pere, file);
				} else {
					int index = n.parent.children.indexOf(n);
					if (index != n.parent.children.size() - 1) {
						Node frere = n.parent.children.get(index + 1);
						setEtatValue(frere, Node.Etat.Vivant, e);
						file.add(frere);
					} else {
						Node pere = setEtatValue(n.parent, Node.Etat.Resolu, e);
						file.add(pere);
					}
				}
			}
		}

		return getMove(bestNode);
	}

	private static Node setEtatValue(Node n, Node.Etat etat, double e) {
		n.setEtat(etat);
		n.setSssValue(e);
		return n;
	}

	private static void remove(Node n, FIFO fifo) {
		ArrayList<Node> removes = new ArrayList<>();
		for (Node g : fifo.fifo) {
			if (Node.isAnscentre(n, g)) {
				removes.add(g);
			}
		}
		for (Node rem : removes) {
			fifo.remove(rem);
		}
	}

	private Move getMove(Node bestNode) {
		if (bestNode.parent.parent == null) {
			return bestNode.last;
		} else {
			return getMove(bestNode.parent);
		}
	}

	public void printTree(Node parent, String ident) {
		System.out.println(ident + parent.name + "(" + parent.turn + ")");
		/*
		 * if (parent.parent == null) { System.out.println(); }else {
		 * System.out.println(" p "+parent.parent.name); }
		 */
		for (Node n : parent.children) {
			printTree(n, ident + " ");
		}
	}

}
