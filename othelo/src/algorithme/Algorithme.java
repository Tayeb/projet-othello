package algorithme;

import Game.Move;
import Game.Node;
import Game.Game;

public abstract class Algorithme {

	private final double POSITIONWEIGHT = 5;
	private final double MOBILITYWEIGHT = 15;
	private final double ENDWEIGHT = 300;	
	protected final double INFINITE = 10000000;

	
	abstract public Move execute(Node game, int player);
	
	protected double checkValue(Node check, int color) {
		double resultat = 0;
		if (check.end == Game.END)
		{
			resultat = color * (-check.position * POSITIONWEIGHT + check.mobility * MOBILITYWEIGHT);
				
		}
		else if (check.end == color)
		{
			resultat =  ENDWEIGHT;
		}
		else if (check.end != color)
		{
			resultat =  -ENDWEIGHT;
		}
		return resultat;
	}	
}