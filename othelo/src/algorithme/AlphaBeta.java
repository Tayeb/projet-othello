package algorithme;
import Game.Move;
import Game.Node;

public class AlphaBeta extends Algorithme {

	@Override
	public Move execute(Node root, int player) {

		return alphaBeta(root, player, -INFINITE, INFINITE);
	}

	private Move alphaBeta(Node root, int player, double alpha, double beta) {
		Node bestNode = null;

		// Check every node in children
		for (Node n : root.children) {
			// Find the node with the best value
			if (alpha < beta) {
				double tempMin = minValue(n, player, alpha, beta);
				if (bestNode == null) {
					alpha = tempMin;
					bestNode = n;
				} else if (tempMin > alpha) {
					alpha = tempMin;
					bestNode = n;
				}
			} else {
				break;
			}
		}

		return bestNode.last;
	}

	private double minValue(Node check, int player, double alpha, double beta) {

		// Check to see if this Node is a leaf
		if (check.children.size() == 0) {
			// Check to see if the game stored in this Node has ended
			return checkValue(check, player);
		}

		// Look for the node among the children with the least value
		for (Node n : check.children) {
			if (alpha < beta) {
				beta = Math.min(beta, maxValue(n, player, alpha, beta));
			}
		}

		return beta;
	}

	private double maxValue(Node check, int player, double alpha, double beta) {

		// Check to see if this Node is a leaf
		if (check.children.size() == 0) {
			return checkValue(check, player);
		}

		// Look for the node among the children with the least value
		for (Node n : check.children) {
			if (alpha < beta) {
				alpha = Math.max(alpha, minValue(n, player, alpha, beta));
			}
		}

		return alpha;
	}
}
