package algorithme;

import java.util.ArrayList;
import java.util.List;

import Game.Node;

public class FIFO {

	List<Node> fifo;


	public FIFO() {
		fifo = new ArrayList<Node>();
	}

	public void add(Node node) {
		int size = fifo.size();
		int i = 0;

		if (node.getEtat() == Node.Etat.Resolu)
			for (i = 0; i < size; i++) {
				Node n = fifo.get(i);
				if (n.getEtat() == Node.Etat.Resolu && node.getSssValue() > n.getSssValue()) {
					break;
				}
			}
		else {
			for (i = 0; i < size; i++) {
				if (fifo.get(i).getEtat() == Node.Etat.Resolu) {
					break;
				}
			}
		}
		fifo.add(i, node);
	}

	public Node getFirst() {
		Node tmp = fifo.get(0);
		fifo.remove(0);
		return tmp;
	}
	
	public void remove(Node n) {
		fifo.remove(n);
	}
	
	public Node.Etat getEtatFirst() {
		return fifo.get(0).getEtat();
	}
	
	public int getValueFirst() {
		return fifo.get(0).name;
	}

	public void print() {
		for (Node n : fifo) {
			System.out.print(n.toTuple() + " ");
		}
		System.out.println(" ");
	}


}
