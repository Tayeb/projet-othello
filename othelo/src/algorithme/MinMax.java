package algorithme;
import Game.Move;
import Game.Node;

public class MinMax extends Algorithme {

	
	/**
	 *  Uses minimax algorithm to determine a move
     *
     *  @param    root    the root of the tree to be checked
	 *	@param	  color   the player who is making the decision
	 *
	 *  @return   best    the best course of action as indicated by minimax
	 */
	@Override
	public Move execute(Node root, int player) {
		

		double max = 0;
		Node bestNode = null;
		
		for (Node n : root.children)
		{
			double tempMin = minValue(n, player);
			if (bestNode == null)
			{
				max = tempMin;
				bestNode = n;
			}
			else if (tempMin > max)
			{
				max = tempMin;
				bestNode = n;
			}
		}
		
		return bestNode.last;
	}
		

	private double minValue(Node check, int player)
	{
		double min = INFINITE;
		
		if (check.children.size() == 0)
		{
			return checkValue(check, player);
		}
		
		for (Node n : check.children)
		{
			double tempMin = maxValue(n, player);
			if (tempMin < min)
			{
				min = tempMin;
			}
		}
		
		return min;
	}
	

	private double maxValue(Node check, int color)
	{
		double max = -INFINITE;
		
		if (check.children.size() == 0)
		{
			return checkValue(check, color);
		}
		
		for (Node n : check.children)
		{
			double tempMax = minValue(n, color);
			if (tempMax > max)
			{
				max = tempMax;
			}
		}
		
		return max;
	}

}
