package algorithme;
import Game.Move;
import Game.Node;

public class NegaMax extends Algorithme {

	@Override
	public Move execute(Node root, int player) {
		Node bestNode = null;
		double max = -INFINITE;
		for (Node n : root.children) {
				double tempVal = -negaMax(n, player);
				if (bestNode == null) {
					max = tempVal;
					bestNode = n;
				} else if (tempVal > max) {
					max = tempVal;
					bestNode = n;
				}
		}
		return bestNode.last;
	}
	
	private double negaMax(Node check, int player) {
		double max = -INFINITE;
		if (check.children.size() == 0) {
			return checkValue(check, player);
		}

		for (Node n : check.children) {
			max = Math.max(max, -negaMax(n, player));
		}

		return max;	
	
	}

}
