package algorithme;
import Game.Move;
import Game.Node;

public class NegaAlphaBeta extends Algorithme {

	@Override
	public Move execute(Node root, int player) {
		return negaAlphaBeta(root, player, -INFINITE, INFINITE);
	}

	private Move negaAlphaBeta(Node root, int player, double alpha, double beta){
		Node bestNode = null;
		double val = -INFINITE;
		// Check every node in children
		for (Node n : root.children) {
			// Find the node with the best value
			if (alpha < beta) {
				val = Math.max(val, -auxNega(n, player, -beta, -alpha));
				if (bestNode == null) {
					alpha = val;
					bestNode = n;
				} else if (val > alpha) {
					alpha = val;
					bestNode = n;
				}
			} else {
				break;
			}
		}
		
		beta = Math.max(-alpha, -beta);

		return bestNode.last;	
	}
	
	private double auxNega(Node check, int player, double alpha, double beta) {
		double val = -INFINITE;
		if (check.children.size() == 0) {
			// Check to see if the game stored in this Node has ended
			return checkValue(check, player);
		}

		// Look for the node among the children with the least value
		for (Node n : check.children) {
			if (alpha < beta) {
				val = Math.max(val,-auxNega(n, player, -beta, -alpha));
				alpha = Math.max(val, alpha);
			}
			beta = Math.max(-alpha, -beta);
		}

		return beta;	
	
	}
}
