package Strategy;

import Game.FillPoints;
import Game.Game;
import Game.Move;

public class PointStrategy implements Strategy {

	
	
	private FillPoints fillpoints;
	
	
	@Override
	public Game strategy(Game game, boolean done, int color) {
		// TODO Auto-generated method stub
		fillpoints = FillPoints.getInstance();
		return pointStrategy(game, done, color);
	}
	
	
    /**
     *  Take a turn using a point based strategy
     *
     *  @param    game    the current state of the game
     *  @param    done    true if the player cannot move anywhere
     *  @param    color   the color (Black or White) of the player
     *
     *  @return   game    the resulting state of the game
     */
    public Game pointStrategy(Game game, boolean done, int color) 
	{
		if (!done)
		{
			Move bestMove = new Move();
			Move currentMove = new Move();
			
			// Look for every legal move
			for (int i = 1; i <= Game.HEIGHT-2; i++)
			{
				for (int j = 1; j <= game.WIDTH-2; j++)
				{
					// Store the current move being checked
					currentMove = game.pointMove(j, i, color, false, fillpoints.pointTable);
					
					// If it is legal
					if (currentMove.legal)
					{
						// If the Move being checked has a higher point score than
						// the best Move, or if the best Move has not been assigned
						if (currentMove.points > bestMove.points || !bestMove.legal)
						{
							bestMove = currentMove;
						}
					}
				}
			}
			
			if (bestMove.legal)
			{
				game.pointMove(bestMove.y, bestMove.x, color, true, fillpoints.pointTable);
				game.board[bestMove.y][bestMove.x] = color;
			}
		}

        return game;
    }

}
