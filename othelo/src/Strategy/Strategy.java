package Strategy;

import Game.Game;

public interface Strategy {

	public Game strategy(Game game, boolean done, int color);

}
