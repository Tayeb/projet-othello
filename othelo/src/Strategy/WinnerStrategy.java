package Strategy;

import Game.FillPoints;
import Game.Game;
import Game.Move;
import Game.Node;
import algorithme.Algorithme;

public class WinnerStrategy implements Strategy{
	
	// Used to determine how large search trees should be
	// Depth 3 actually performs worse, there is some kind of logic error in my algorithm
	// Depth 4 doesn't even return anything, I can't tell if it's because of the exponential growth or if there's a bug somewhere
	int MAXDEPTH = 5;
	private FillPoints fillpoints;

	private Algorithme algorithme;
	
	public WinnerStrategy(Algorithme algorithme, int depth) {
		this.algorithme = algorithme;
		this.MAXDEPTH = depth;
	}
	
	
	@Override
	public Game strategy(Game game, boolean done, int color) {
		fillpoints = FillPoints.getInstance();
        return searchStrategy(game,done,color);
	}
	
	/**
     *  Take a turn using a search based strategy that makes use of cell points
     *
     *  @param    game    the current state of the game
     *  @param    done    true if the player cannot move anywhere
     *  @param    color   the color (Black or White) of the player
     *
     *  @return   game    the resulting state of the game
     */
	private Game searchStrategy(Game game, boolean done, int color) 
	{	
		if (!done)
		{
			Node currentState = new Node(new Move(), game, color);
			currentState = buildTree(currentState, MAXDEPTH);
			//printTree(currentState);
			
			Move bestMove = algorithme.execute(currentState, color);
			//System.out.println("Best move is " + testMove.x + " " + testMove.y);
			
			if (bestMove.legal)
			{
				game.pointMove(bestMove.y, bestMove.x, color, true, fillpoints.pointTable);
				game.board[bestMove.y][bestMove.x] = color;
			}
		}
		
		return game;
	}
	
	/**
     *  Creates a tree to explore potential moves
     *
	 * 	@param    parent   The node to expand 
     *  @param    depth    A counter to keep track of the depth of the tree.
	 *
	 *  @return   parent   The parent with any new children
     */
	public Node buildTree(Node parent, int depth)
	{
		// Check to see if there is still more to expand and if the game isn't done
		if (depth > 0 && parent.end == Game.END)
		{
			// Decrease the depth count
			depth--;
			
			// Determine the subsequent turn ahead of time
			int nextTurn;
			if (parent.turn == Game.BLACK) 
				nextTurn = Game.WHITE;
			else 
				nextTurn = Game.BLACK;
			
			// Searches for legal moves
			for (int i = 1; i <= Game.HEIGHT-2; i++)
			{
				for (int j = 1; j <= Game.WIDTH-2; j++)
				{
					// Store the current move being checked
					Move currentMove = parent.state.pointMove(i, j, parent.turn, false, fillpoints.pointTable);
					
					// If it is legal
					if (currentMove.legal)
					{
						//if (parent.turn == Game.BLACK) 
						//	System.out.println("BLACK (" + parent.turn + ") turn");
						//else 
						//	System.out.println("WHITE (" + parent.turn + ") turn");
						//System.out.println(j + " " + i + " is a legal move");
						
						// Create a Game object that attempts the current move
						Game futureGame = new Game(parent.state);
						futureGame = makeMove(futureGame, false, parent.turn, currentMove);
						
						// Create a Node that holds the current move, the future game
						Node newNode = new Node(currentMove, futureGame, nextTurn);
						
						// The position value of the new node is the number of points gained by the move leading up to that node
						newNode.position = currentMove.points;
						// Check the number of potential moves of the future game
						newNode.mobility = mobilityCheck(futureGame, nextTurn);
						// Checked whether or not the future game has ended
						newNode.end =  endCheck(futureGame);

						parent.addChild(newNode);
						newNode.parent = parent;
					}
				}
			}
		
			// Check again to see if there is still more to expand
			if (depth > 0)
			{
				// Build a sub tree for each child
				for (Node n : parent.children)
				{
					n = buildTree(n, depth);
				}
			}
			
			// Calculate mobility while recursing backwards
			parent.mobility = parent.children.size();
		}
		return parent;
	}
	
	/**
     *  Take a turn using a given game and a move
     *
     *  @param    game    the current state of the game
     *  @param    done    true if the player cannot move anywhere
     *  @param    color   the color (Black or White) of the player
     *
     *  @return   game    the resulting state of the game
     */
    public Game makeMove(Game game, boolean done, int color, Move move) 
	{
		if (!done)
		{
			if (move.legal)
			{
				game.pointMove(move.y, move.x, color, true, fillpoints.pointTable);
				game.board[move.y][move.x] = color;
			}
		}

        return game;
    }
	
	/**
     *  Checks to see how many potential moves can be made from this game
     *
     *  @param    game    the current state of the game
     *  @param    color   the player whose turn it is
	 *
     *  @return   result  the number of moves the player specified by color can make
     */
    public int mobilityCheck(Game game, int color) 
	{
		int result = 0;
		
		for (int i=1; i<game.HEIGHT-1; i++)
		{
            for (int j=1; j<game.WIDTH-1; j++)
			{
				if ((game.legalMove(i, j, color, false)))
				{
                    result++;
				}
			}
		}

        return result;
    }
	
	/**
     *  Checks to see if a given game is finished
     *
     *  @param    game    the current state of the game
     *
     *  @return   result  -1 if game is unfinished, 0 in case of a tie or either 1 or 2 depending on which player is the winner
     */
    public int endCheck(Game game) 
	{
		int result = Game.END;
		int whiteSum = 0;
		int blackSum = 0;
		
		for (int i=1; i<game.HEIGHT-1; i++)
		{
            for (int j=1; j<game.WIDTH-1; j++)
			{
				if ((game.legalMove(i, j, Game.BLACK, false)) ||
                   (game.legalMove(i, j, Game.WHITE, false)))
				{
                    result = Game.END;
					return result;
				}
				
				if (game.board[i][j] == Game.BLACK)
				{
					blackSum++;
				}
				else if (game.board[i][j] == Game.WHITE)
				{
					whiteSum++;
				}
			}
		}

		if (blackSum > whiteSum)
		{
			result = Game.BLACK;
		}
		else if (whiteSum > blackSum)
		{
			result = Game.WHITE;
		}
		else
		{
			result = 0;
		}
		
        return result;
    }
	

}
